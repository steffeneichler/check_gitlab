# changes by release

## 0.4.0
### features
* new license checks - license-expire and license-overage

### other
* license file added
* rubocop - rescue exception var name

## 0.3.2
### fixes
* ci-pipeline: added *finished* scope in api call to avoid duration "null" exception (#7)
* ci-runner: added perf data
* group_size: remove obsolete method call

## 0.3.1
### fixes
* check for correct sudoers entry (#5)

## 0.3
### features
* new mode - ci runner jobs duration: checks duration of first found running job on a ci runner

### other
* health mode - access token has been removed due to deprecation. use [IP whitelists](https://docs.gitlab.com/ee/user/admin_area/monitoring/health_check.html#ip-whitelist) instead.

## 0.2
### features
* new mode - group size: checks size of group/subgroup
* debug/verbose output for health check (!3)

### fixes
* catch all non 200 code responses as unknown status instead of a few selected (!4)

### other
* add helper methods and refactor some code
* empty search response produces an unknown status

## 0.1.1
* using v4 api endpoint
* health mode: using readiness probes endpoint instead of deprecated health_check - https://docs.gitlab.com/ce/user/admin_area/monitoring/health_check.html#using-the-endpoint

### fixes
* pipeline mode: only reports with status 'success'. running and skipped pipelines will not generate an error anymore.

## 0.1
* Initial release
